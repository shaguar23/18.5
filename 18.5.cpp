﻿#include <iostream>   
#include <ostream>
#include <utility> 

using namespace std;
class player
{
    string name;
    int score;

public:
    int GetScore()
    {
        return score;
    }

    void Score()
    {
        cout << "Введите имя персонажа: ";
        cin >> name;
        cout << "Введите очки персонажа: ";
        cin >> score;
    }

    void Show()
    {
        cout << name << " " << score << "\n";
    }

    void bubbleSort(player* Players, int n)
    {

        for (int i = 0; i < n - 1; i++)
        {
            for (int j = 0; j < n - i - 1; j++)
            {
                if (Players[j].score > Players[j + 1].score)
                {
                    swap(Players[j], Players[j + 1]);
                }

            }

        }

    }
    void printArray(player* Players, int n)
    {
        cout << "Таблица игроков:\n";
        for (int i = 0; i < n; i++)
        {
            cout << Players[i].name << " ---- " << Players[i].score << "\n";
        }

        cout << endl;
    }
};



int main() {

    setlocale(LC_ALL, "Russian");
    int n;
    cout << "Введите колличество игроков: ";
    cin >> n;
    player* Players = new player[n];

    for (int j = 0; j < n; ++j)
    {
        Players[j].Score();
    }

    player start;
    start.bubbleSort(Players, n);
    start.printArray(Players, n);
    delete[] Players;
    
}
